package Tab;
import javax.persistence.*;

/**
 * Created by Admin on 22/06/2017.
 */
@Entity
@Table(name="employees")
public class Employees extends Temp{
    @Id
    @Column(name="employeenumber")
    private int EmployeeNumber;

    @Column(name="lastname")
    private String LastName;

    @Column(name="firstname")
    private String FirstName;

    @Column(name="extension")
    private String Extension;

    @Column (name="email")
    private String Email;

    @ManyToOne

    @JoinColumn(name="officecode")
    private Offices offices;

    @ManyToOne
    @JoinColumn(name="reportsto")
    private Customers customers;

    @Column(name="jobtitle")
    private String JobTitle;

    public Employees(){}
    public Employees(int employeeNumber, String lastName, String firstName, String extension, String email, Offices offices, Customers customers, String jobTitle) {
        EmployeeNumber = employeeNumber;
        LastName = lastName;
        FirstName = firstName;
        Extension = extension;
        Email = email;
        this.offices = offices;
        this.customers = customers;
        JobTitle = jobTitle;
    }

    public int getEmployeeNumber() {
        return EmployeeNumber;
    }

    public void setEmployeeNumber(int employeeNumber) {
        EmployeeNumber = employeeNumber;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getExtension() {
        return Extension;
    }

    public void setExtension(String extension) {
        Extension = extension;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public Offices getOffices() {

        return offices;
    }

    public void setOffices(Offices offices) {
        this.offices = offices;
    }

    public Customers getCustomers() {
        return customers;
    }

    public void setCustomers(Customers customers) {
        this.customers = customers;
    }

    public String getJobTitle() {
        return JobTitle;
    }

    public void setJobTitle(String jobtitle) {
        JobTitle = jobtitle;
    }
}
