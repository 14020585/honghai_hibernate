package Tab;
import com.sun.org.apache.xpath.internal.operations.Or;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Admin on 22/06/2017.
 */
@Entity
@Table(name = "orders")
public class Orders extends Temp{
    @Id
    @Column(name = "ordernumber")
    private int OrderNumber;

    @Column(name = "orderdate")
    private Date OrderDate;

    @Column(name = "requireddate")
    private Date RequiredDate;

    @Column(name = "shippeddate")
    private Date ShippedDate;

    @Column(name = "status")
    private String Status;

    @Column(name = "comments")
    private String Comment;

    @ManyToOne
    @JoinColumn(name = "customernumber")
    private Customers customers;

    public Orders(){}

    public Orders(int orderNumber, Date orderDate, Date requiredDate, Date shippedDate, String status, String comment, Customers customers) {
        OrderNumber = orderNumber;
        OrderDate = orderDate;
        RequiredDate = requiredDate;
        ShippedDate = shippedDate;
        Status = status;
        Comment = comment;
        this.customers = customers;
    }

    public int getOrderNumber() {
        return OrderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        OrderNumber = orderNumber;
    }

    public Date getOrderDate() {
        return OrderDate;
    }

    public void setOrderDate(Date orderDate) {
        OrderDate = orderDate;
    }

    public Date getRequiredDate() {
        return RequiredDate;
    }

    public void setRequiredDate(Date requiredDate) {
        RequiredDate = requiredDate;
    }

    public Date getShippedDate() {
        return ShippedDate;
    }

    public void setShippedDate(Date shippedDate) {
        ShippedDate = shippedDate;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public Customers getCustomers() {
        return customers;
    }

    public void setCustomers(Customers customers) {
        this.customers = customers;
    }
}
