package Tab;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Admin on 22/06/2017.
 */
@Embeddable
public class OrderDetailsPK extends Temp implements Serializable{

    @ManyToOne
    @JoinColumn(name = "ordernumber")
    private Orders orders;


    @ManyToOne
    @JoinColumn(name = "productcode")
    private Products products;

    public OrderDetailsPK(){}

    public OrderDetailsPK(Orders orders, Products products) {
        this.orders = orders;
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderDetailsPK)) return false;

        OrderDetailsPK that = (OrderDetailsPK) o;

        if (!orders.equals(that.orders)) return false;
        return products.equals(that.products);
    }

    @Override
    public int hashCode() {
        int result = orders.hashCode();
        result = 31 * result + products.hashCode();
        return result;
    }
}


