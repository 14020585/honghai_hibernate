package Tab;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Admin on 22/06/2017.
 */
@Entity
@Table(name = "productlines")
public class ProductLines extends Temp{
    @Id
    @Column(name = "productline")
    private String ProductLine;

    @Column(name = "textdescription")
    private String TextDescription;

    @Column(name = "htmldescription")
    private String HTMLDescription;

    @Column(name = "image")
    private Serializable Image;

    public ProductLines(){}

    public ProductLines(String productLine, String textDescription, String HTMLDescription, Serializable image) {
        ProductLine = productLine;
        TextDescription = textDescription;
        this.HTMLDescription = HTMLDescription;
        Image = image;
    }

    public String getProductLine() {
        return ProductLine;
    }

    public void setProductLine(String productLine) {
        ProductLine = productLine;
    }

    public String getTextDescription() {
        return TextDescription;
    }

    public void setTextDescription(String textDescription) {
        TextDescription = textDescription;
    }

    public String getHTMLDescription() {
        return HTMLDescription;
    }

    public void setHTMLDescription(String HTMLDescription) {
        this.HTMLDescription = HTMLDescription;
    }

    public Serializable getImage() {
        return Image;
    }

    public void setImage(Serializable image) {
        Image = image;
    }
}
