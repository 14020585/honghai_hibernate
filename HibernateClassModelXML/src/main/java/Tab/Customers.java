package Tab;
import javax.persistence.*;

/**
 * Created by Admin on 22/06/2017.
 */
@Entity
@Table(name="customers")
public class Customers extends Temp{
    @Id
    @Column(name="customernumber")
    private int CustomerNumber;

    @Column(name="customername")
    private String CustomerName;

    @Column(name="contactlastname")
    private String ContactLastname;

    @Column(name="contactfirstname")
    private String ContactFirstName;

    @Column(name="phone")
    private String Phone;

    @Column(name="addressline1")
    private String AddressLine1;

    @Column(name="addressline2")
    private String AddressLine2;

    @Column(name="city")
    private String City;

    @Column(name="state")
    private String State;

    @Column(name = "postalcode")
    private String PostalCode;

    @Column(name = "country")
    private String Country;

    @ManyToOne
    @JoinColumn(name = "salesrepemployeenumber")
    private Employees SaleRepEmployee;

    @Column(name="creditlimit")
    private double CreditLimit;

    public Customers(){}

    public Customers(int customerNumber, String customerName, String contactLastname,
                     String contactFirstName, String phone, String addressLine1, String addressLine2,
                     String city, String state, String postalCode, String country, int saleRepEmployeeNumber, double creditLimit){

        this.CustomerNumber = customerNumber;
        this.CustomerName = customerName;
        this.ContactLastname = contactLastname;
        this.ContactFirstName = contactFirstName;
        this.Phone = phone;
        this.AddressLine1 = addressLine1;
        this.AddressLine2 = addressLine2;
        this.City = city;
        this.State = state;
        this.PostalCode = postalCode;
        this.Country = country;
        this.CreditLimit = creditLimit;

    }

    public int getCustomerNumber() {
        return CustomerNumber;
    }

    public void setCustomerNumber(int customerNumber) {
        CustomerNumber = customerNumber;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getContactLastname() {
        return ContactLastname;
    }

    public void setContactLastname(String contactLastname) {
        ContactLastname = contactLastname;
    }

    public String getContactFirstName() {
        return ContactFirstName;
    }

    public void setContactFirstName(String contactFirstName) {
        ContactFirstName = contactFirstName;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getAddressLine1() {
        return AddressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        AddressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return AddressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        AddressLine2 = addressLine2;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(String postalCode) {
        PostalCode = postalCode;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public Employees getSaleRepEmployee() {
        return SaleRepEmployee;
    }

    public void setSaleRepEmployee(Employees saleRepEmployee) {
        SaleRepEmployee = saleRepEmployee;
    }

    public double getCreditLimit() {
        return CreditLimit;
    }

    public void setCreditLimit(double creditLimit) {
        CreditLimit = creditLimit;
    }
}
