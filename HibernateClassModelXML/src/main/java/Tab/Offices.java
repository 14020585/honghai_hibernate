package Tab;

import javax.persistence.*;

/**
 * Created by Admin on 22/06/2017.
 */
@Entity
@Table(name = "offices")
public class Offices extends Temp{
    @Id
    @Column(name = "officecode")
    private String OfficeCode;

    @Column(name = "city")
    private String City;

    @Column(name = "phone")
    private String Phone;

    @Column(name = "addressline1")
    private String AddressLine1;

    @Column(name = "addressline2")
    private String AddressLIne2;

    @Column(name = "state")
    private String State;

    @Column(name = "country")
    private String Country;

    @Column(name = "postalcode")
    private String PostalCode;

    @Column(name = "territory")
    private String Territory;

    public Offices(){}
    public Offices(String officeCode, String city, String phone, String addressLine1, String addressLIne2, String state, String country, String postalCode, String territory) {
        OfficeCode = officeCode;
        City = city;
        Phone = phone;
        AddressLine1 = addressLine1;
        AddressLIne2 = addressLIne2;
        State = state;
        Country = country;
        PostalCode = postalCode;
        Territory = territory;
    }

    public String getOfficeCode() {
        return OfficeCode;
    }

    public void setOfficeCode(String officeCode) {
        OfficeCode = officeCode;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getAddressLine1() {
        return AddressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        AddressLine1 = addressLine1;
    }

    public String getAddressLIne2() {
        return AddressLIne2;
    }

    public void setAddressLIne2(String addressLIne2) {
        AddressLIne2 = addressLIne2;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(String postalCode) {
        PostalCode = postalCode;
    }

    public String getTerritory() {
        return Territory;
    }

    public void setTerritory(String territory) {
        Territory = territory;
    }
}
