import Tab.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.annotations.SourceType;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 23/06/2017.
 */
public class Application {

    public static SessionFactory sessionFactory;
    public static void saveDB(Temp temp){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.save(temp);
        transaction.commit();

        session.close();

    }

    public static void deleteCustomer(int customernumber){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Customers customers = (Customers) session.get(Customers.class, customernumber);
        session.delete(customers);
        transaction.commit();
        session.close();

    }

    public static void updateCountryCustomer(int customernumber, String country){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Customers customer = (Customers) session.get(Customers.class, customernumber);
        customer.setCountry(country);

        session.update(customer);
        transaction.commit();
        session.close();
    }

    public static void displayAllCustomers(){
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        List<Customers> customers = new ArrayList<Customers>();


        customers = (List<Customers>) session.createQuery("from Customers").list();
        transaction.commit();
        session.close();

        for (int i=0; i < customers.size(); i ++){
            System.out.println("Customer's number: " + customers.get(i).getCustomerNumber() + " "
                    + "\nCustomer's name: " + customers.get(i).getCustomerName() + " "
                    + "\nCustomer's city: " + customers.get(i).getCity() + " "
                    + "\nCustomer's country: " + customers.get(i).getCountry() + "\n--------");
        }

    }

    public static void showCustomers(Customers customers){
        System.out.println("Customer Number: " + customers.getCustomerNumber() +"\n"
                + "Customer Name: " + customers.getCustomerName() + "\n"
                + "Phone: " + customers.getPhone());
    }

    public static void main(String args[]){

        Configuration configuration = new Configuration().configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);

        // insert into table 'customers'
        Customers c1 = new Customers(1, "Le Minh", "Minh", "Le", "0974622702", "Xuan Thuy",
                "Cau Giay", "Ha Noi", "Cau Giay", "A03", "Viet Nam", 2, 4.5);
        Customers c2 = new Customers(2, "Nguyen Van A", "A", "Nguyen", "0974633702", "Me Tri",
                "Pham Hung", "Ha Noi", "Ba Dinh", "A04", "Viet Nam", 1, 2.5);
        Customers c3 = new Customers(3, "Hong Ngoc", "Ngoc", "Le", "0974722902", "Xuan Thuy",
                "Tran Quoc Hoan", "Ha Noi", "Cau Giay", "A05", "Viet Nam", 1, 5.5);
        Customers c4 = new Customers(4, "Ngo Kinh", "Kinh", "Ngo", "0974600702", "Xuan Thuy",
                "Ho Tung Mau", "Ha Noi", "Cau Giay", "A06", "Viet Nam", 2, 7.5);
        Customers c5 = new Customers(5, "Le Giang", "Giang", "Le", "09745622702", "Xuan Thuy",
                "Duong Quang Ham", "Ha Noi", "Cau Giay", "A07", "Viet Nam", 2, 1.5);

        saveDB(c1);
        saveDB(c2);
        saveDB(c3);
        saveDB(c4);
        saveDB(c5);

        //insert into table 'offices'
        Offices o1 = new Offices("A123", "HCM", "+841233075", "Ha Noi", "Viet Nam", "Quan Go Vap", "HCM", "H1H", "dunno");

        saveDB(o1);

        //insert into table 'employees'
        Employees e1 = new Employees(1, "Ryan", "Avery", "abc", "ryan@gmail.com", o1, c1, "President");

        saveDB(e1);

        //insert into table 'productlines'
        ProductLines pl1 = new ProductLines("A30", "qwertyuiop", "oop", "E:\\Hải\\PicArt\\1SCcsz.jpg");

        saveDB(pl1);

        //insert into table 'products'
        Products p1 = new Products("F1", "Samsung Galaxy J1", pl1, "a", "b", "1234acbd", 3, 30000, 9.0);

        saveDB(p1);

        //insert into table 'orders'
        Orders or1 = new Orders(23, null, null, null, "shipped", "good", c1);

        saveDB(or1);
//---------------------
        //insert into table 'payments'
        PaymentsPK pmk1 = new PaymentsPK(c1, "N2");
        Payments pm1 = new Payments();
        pm1.setPaymentsPK(pmk1);
        saveDB(pm1);

        //insert into table 'orderdetails'
        OrderDetailsPK odk1 = new OrderDetailsPK(or1, p1);
        OrderDetails od1 = new OrderDetails();
        od1.setOrderDetailsPK(odk1);
        saveDB(od1);

        //deleteCustomer(8);

        //updateCountryCustomer(1, "VN");
        displayAllCustomers();
        //showCustomers(c1);
        //showCustomers(c2);

    }

}
