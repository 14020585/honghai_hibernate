package EntityClasses;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Admin on 23/06/2017.
 */
@Entity
@Table(name = "orderdetails")
public class OrderDetails extends Temp implements Serializable {
    @EmbeddedId
    private OrderDetailsPK orderDetailsPK;

    @Column(name = "quantityordered")
    private int QuantityOrderd;

    @Column(name = "priceeach")
    private double PriceEach;

    @Column(name = "orderlinenumber")
    private int OrderLineNumber;

    public OrderDetails(){}

    public OrderDetails(OrderDetailsPK orderDetailsPK, int quantityOrderd, double priceEach, int orderLineNumber) {
        this.orderDetailsPK = orderDetailsPK;
        QuantityOrderd = quantityOrderd;
        PriceEach = priceEach;
        OrderLineNumber = orderLineNumber;
    }

    public OrderDetailsPK getOrderDetailsPK() {
        return orderDetailsPK;
    }

    public void setOrderDetailsPK(OrderDetailsPK orderDetailsPK) {
        this.orderDetailsPK = orderDetailsPK;
    }

    public int getQuantityOrderd() {
        return QuantityOrderd;
    }

    public void setQuantityOrderd(int quantityOrderd) {
        QuantityOrderd = quantityOrderd;
    }

    public double getPriceEach() {
        return PriceEach;
    }

    public void setPriceEach(double priceEach) {
        PriceEach = priceEach;
    }

    public int getOrderLineNumber() {
        return OrderLineNumber;
    }

    public void setOrderLineNumber(int orderLineNumber) {
        OrderLineNumber = orderLineNumber;
    }
}
