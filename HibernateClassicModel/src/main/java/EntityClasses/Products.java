package EntityClasses;

import javax.persistence.*;

/**
 * Created by Admin on 22/06/2017.
 */
@Entity
@Table(name = "products")
public class Products extends Temp{
    @Id
    @Column(name = "productcode")
    private String ProductCode;

    @Column(name = "productname")
    private String ProductName;

    @OneToOne
    @JoinColumn(name = "productline")
    private ProductLines productLines;

    @Column(name = "productscale")
    private String ProductScale;

    @Column(name = "productvendor")
    private String ProductVendor;

    @Column(name = "productdescription")
    private String ProductDescription;

    @Column(name = "quantityinstock")
    private int QuantityInStock;

    @Column(name = "buyprice")
    private double BuyPrice;

    @Column(name = "msrp")
    private double MSRP;

    public Products(){}

    public Products(String productCode, String productName, ProductLines productLines, String productScale, String productVendor, String productDescription, int quantityInStock, double buyPrice, double MSRP) {
        ProductCode = productCode;
        ProductName = productName;
        this.productLines = productLines;
        ProductScale = productScale;
        ProductVendor = productVendor;
        ProductDescription = productDescription;
        QuantityInStock = quantityInStock;
        BuyPrice = buyPrice;
        this.MSRP = MSRP;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String productCode) {
        ProductCode = productCode;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getProductScale() {
        return ProductScale;
    }

    public void setProductScale(String productScale) {
        ProductScale = productScale;
    }

    public String getProductVendor() {
        return ProductVendor;
    }

    public void setProductVendor(String productVendor) {
        ProductVendor = productVendor;
    }

    public String getProductDescription() {
        return ProductDescription;
    }

    public void setProductDescription(String productDescription) {
        ProductDescription = productDescription;
    }

    public int getQuantityInStock() {
        return QuantityInStock;
    }

    public void setQuantityInStock(int quantityInStock) {
        QuantityInStock = quantityInStock;
    }

    public double getBuyPrice() {
        return BuyPrice;
    }

    public void setBuyPrice(double buyPrice) {
        BuyPrice = buyPrice;
    }

    public double getMSRP() {
        return MSRP;
    }

    public void setMSRP(double MSRP) {
        this.MSRP = MSRP;
    }

    public ProductLines getProductLines() {
        return productLines;
    }

    public void setProductLines(ProductLines productLines) {
        this.productLines = productLines;
    }
}
