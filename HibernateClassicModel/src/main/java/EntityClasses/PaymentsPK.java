package EntityClasses;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Admin on 22/06/2017.
 */

@Embeddable
public class PaymentsPK extends Temp implements Serializable{
    @OneToOne
    @JoinColumn(name = "customernumber")
    private Customers customers;

    @Column(name = "checknumber")
    private String CheckNumber;

    public PaymentsPK(){}

    public PaymentsPK(Customers customers, String checkNumber) {
        this.customers = customers;
        CheckNumber = checkNumber;
    }

    public Customers getCustomers() {
        return customers;
    }

    public void setCustomers(Customers customers) {
        this.customers = customers;
    }

    public String getCheckNumber() {
        return CheckNumber;
    }

    public void setCheckNumber(String checkNumber) {
        CheckNumber = checkNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PaymentsPK)) return false;

        PaymentsPK that = (PaymentsPK) o;

        if (!customers.equals(that.customers)) return false;
        return CheckNumber.equals(that.CheckNumber);
    }

    @Override
    public int hashCode() {
        int result = customers.hashCode();
        result = 31 * result + CheckNumber.hashCode();
        return result;
    }
}

