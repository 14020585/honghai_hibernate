package EntityClasses;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Admin on 23/06/2017.
 */
@Entity
@Table(name = "payments")
public class Payments extends Temp implements Serializable {
    @EmbeddedId
    private PaymentsPK paymentsPK;

    @Column(name = "paymentdate")
    private Date PaymentDate;

    @Column(name = "amount")
    private Double Amount;

    public PaymentsPK getPaymentsPK() {
        return paymentsPK;
    }

    public void setPaymentsPK(PaymentsPK paymentsPK) {
        this.paymentsPK = paymentsPK;
    }

    public Date getPaymentDate() {
        return PaymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        PaymentDate = paymentDate;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }
}
