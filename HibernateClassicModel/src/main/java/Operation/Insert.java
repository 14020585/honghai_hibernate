package Operation;

import EntityClasses.Temp;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.Set;

/**
 * Created by Admin on 22/06/2017.
 */
public class Insert {
    private SessionFactory factory;

    public Insert(SessionFactory factory) {
        this.factory = factory;
    }

    private void saveDB(Temp temp){
        Session session = factory.openSession();
        Transaction transaction= session.beginTransaction();

        try {
            session.save(temp);
            transaction.commit();

        }catch (HibernateException er){
            if (transaction!=null){
                transaction.rollback();
            }

            er.printStackTrace();
        }

        finally {
            session.close();

        }
    }

}
