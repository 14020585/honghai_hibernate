DROP TABLE productlines;
CREATE TABLE productlines (
  productLine VARCHAR(50) NOT NULL PRIMARY KEY ,
  textDescription VARCHAR(4000),
  htmlDescription TEXT,
  image BYTEA
);

DROP TABLE products;
CREATE TABLE products (
  productCode VARCHAR(15) PRIMARY KEY ,
  productName VARCHAR(70),
  productLine VARCHAR(50) REFERENCES productlines(productLine),
  productScale VARCHAR(10),
  productVendor VARCHAR(50),
  productDescription TEXT,
  quantityInStock SMALLINT,
  buyPrice DOUBLE PRECISION,
  MSRP DOUBLE PRECISION
);

DROP TABLE offices;
CREATE TABLE offices (
  officeCode VARCHAR(10) PRIMARY KEY ,
  city VARCHAR(50),
  phone VARCHAR(50),
  addressLine1 VARCHAR(50),
  addressLine2 VARCHAR(50),
  state VARCHAR(50),
  country VARCHAR(50),
  postalCode VARCHAR(15),
  territory VARCHAR(10)
);

DROP TABLE employees;
CREATE TABLE employees (
  employeeNumber INT PRIMARY KEY ,
  lastName VARCHAR(50),
  firstName VARCHAR(50),
  extension VARCHAR(10),
  email VARCHAR(100),
  officeCode VARCHAR(10) REFERENCES offices(officeCode),
  reportsTo INT REFERENCES employees(employeeNumber),
  jobTitle VARCHAR(50)
);

DROP TABLE customers;
CREATE TABLE customers (
  customerNumber INT PRIMARY KEY ,
  customerName VARCHAR(50),
  contactLastName VARCHAR(50),
  contactFirstName VARCHAR(50),
  phone VARCHAR(50),
  addressLine1 VARCHAR(50),
  addressLine2 VARCHAR(50),
  city VARCHAR(50),
  state VARCHAR(50),
  postalCode VARCHAR(15),
  country VARCHAR(50),
  salesRepEmployeeNumber INT REFERENCES employees(employeeNumber),
  creditLimit DOUBLE PRECISION
);

DROP TABLE payments;
CREATE TABLE payments (
  customerNumber INT,
  checkNumber VARCHAR(50),
  paymentDate TIME,
  amount DOUBLE PRECISION,
  PRIMARY KEY (customerNumber, checkNumber)
);

DROP TABLE orders;
CREATE TABLE orders (
  orderNumber INT PRIMARY KEY ,
  orderDate TIME,
  requiredDate TIME,
  shippedDate TIME,
  status VARCHAR(15),
  comments Text,
  customerNumber INT REFERENCES customers(customerNumber)
);

DROP TABLE orderdetails;
CREATE TABLE orderdetails (
  orderNumber INT REFERENCES orders(orderNumber),
  productCode VARCHAR(15) REFERENCES  products(productCode),
  quantityOrdered INT,
  priceEach DOUBLE PRECISION,
  orderLineNumber SMALLINT,
  PRIMARY KEY (orderNumber, productCode)
);

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE public.hibernate_sequence OWNER TO postgres;